import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CommonService {
  constructor(private httpClient: HttpClient) {}

  postData(body) {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.httpClient.post("http://ec2-3-128-171-238.us-east-2.compute.amazonaws.com/", JSON.stringify(body), { headers: httpHeaders })
      .toPromise()
      .then((response: Response) => {
        return response
      })
      .catch(
        (error: Response) => {
          console.log(error)
        })
  }
}