import { Injectable } from '@angular/core';
import { CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

    constructor(private commonService: CommonService) { }

    verifyImage(data){
        return this.commonService.postData(data)
    }
}
