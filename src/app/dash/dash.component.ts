import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent {
  /** Based on the screen size, switch from standard to one column per row */
  getdata:any
  useruid;
  result:boolean;
  countHappy:number=0;
  countSad:number=0;
  userid;
  resultcount;
  countEm:number=0;
  sumYears:number=0;
  AverageYear:number=0;
  cardLayout = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return {
          columns: 1,
          miniCard: { cols: 1, rows: 1 },
          chart: { cols: 1, rows: 2 },
          table: { cols: 1, rows: 4 },
        };
      }
 
     return {
        columns: 4,
        miniCard: { cols: 1, rows: 1 },
        chart: { cols: 2, rows: 2 },
        table: { cols: 4, rows: 4 },
      };
    })
  );

  constructor(private breakpointObserver: BreakpointObserver,private fs:AngularFirestore,private as:AuthService) {
    this.as.getUser().subscribe(user=>{
      this.userid=user.uid
    })
    console.log(this.userid)
    this.fs.collection("employees").snapshotChanges().subscribe((data)=>{
    this.resultcount=data.map(element=>{
          if(element.payload.doc.data()['uidUser']===this.userid){
            this.countEm=this.countEm+1;
            this.sumYears=this.sumYears+element.payload.doc.data()['yearsInCurrentJob'];
            this.AverageYear = this.sumYears/this.countEm;
            console.log(this.sumYears+ "sumYears");
            console.log(this.countEm+ "countEm");
            console.log(this.AverageYear+ "AverageYear");
            if(element.payload.doc.data()['result']>=0.5){
              this.countSad=this.countSad+1;
            }
            else{
              this.countHappy=this.countHappy+1;
            }
            return {
              key:element.payload.doc.id
            }
          }
        
       })
     })

     this.AverageYear = this.sumYears/this.countEm;
    //  +replaceAll(res,'"',"")
  }
}
