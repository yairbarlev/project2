export interface Sale {
    email:string;
    customer:string;
    date:string;
    hour:string;
    weight:number;
    color:string;
    clarity:string;
    shape:number;
    price:number;
    image:string;
    details:string;
    timestamp:number;
}
