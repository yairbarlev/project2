import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {ProfileService} from "../_services/profile.service";

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  imageBase64: any = ""
  image: any = ""
  similer_components = []

  message = ""

  base_url ="https://hr-prediction-image-collection.s3.us-east-2.amazonaws.com/"
  constructor(private cd: ChangeDetectorRef, private profileService: ProfileService) { }

  ngOnInit(): void {
  }


  onFileChange(event) {
    this.similer_components = []
    this.message = ""
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageBase64 = reader.result;
        this.image = reader.result;
        this.cd.markForCheck();
      };
    }
  }

  submitProfile(){
    if(!this.imageBase64){
      return
    }

    console.log(this.imageBase64.split(","))
    let data = {
      image: this.imageBase64.split(",")[1]
    }

    this.message = "Loading..."
    this.profileService.verifyImage(data).then(res=>{
      console.log(res)
      if(res['status'] == "success") {
        this.similer_components = res["data"].filter(res=>res["similarity"] != "0")
        this.message = ""
        if (!this.similer_components.length){
          this.message = "There is no matching records"
        }
        
      } else {
        this.similer_components = []
        this.message = "There is no matching records"
      }
    })
  }

}
