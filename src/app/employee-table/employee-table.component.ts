import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../interfaces/employee';
import { BehaviorSubject, Observable, observable, Subscription } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { PersonFormDialogComponent }  from '../person-form-dialog/person-form-dialog.component';
import { EmployeeService } from '../employee.service';
import { AuthService } from '../auth.service';
import { map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.css'],
})
export class EmployeeTableComponent implements OnInit {
  @ViewChild(MatPaginator)   paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  userId:string;
  employees$: BehaviorSubject<Employee[]>;
  public displayedColumns: string[] = ['firstName', 'lastName', 'age','id','gender','trainingHours'];

  public columnsToDisplay: string[] = [...this.displayedColumns, 'actions'];

  employeess$;
  employees:any  [];
  public columnsFilters = {};
  getEmployee
  dataArray
  dataupdate={
    key:'',
    firstName:'',
    lastName:'',
    age:'',
    city:'',
    gender:'',
    trainingHours:'',
    companySize:'',
    companytype:'',
    educationLevel:'',
    educationYears:'',
    enrollment:'',
    experienceYears:'',
    majorDisciplin:'',
    rExperience:'',
    result:'',
    state:'',
    street:'',
    yearsInCurrentJob:'',

  }
  messageupdate
  constructor(public authService:AuthService ,private router:Router,private EmployeesService: EmployeeService,private fs:AngularFirestore, public dialog: MatDialog) {
    // this.dataSource = new MatTableDataSource<Employee>();
    // this.employees$ = new BehaviorSubject([]);
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
      }
    )

    this.fs.collection("employees").snapshotChanges().subscribe((data)=>{
      this.dataArray= data.map(element=>{

        return{ 
          key:element.payload.doc.id,
          firstName:element.payload.doc.data()['firstName'],
          lastName:element.payload.doc.data()['lastName'],
          age:element.payload.doc.data()['age'],
          gender:element.payload.doc.data()['gender'],
          trainingHours:element.payload.doc.data()['trainingHours'],
          companySize:element.payload.doc.data()['companySize'],
          city:element.payload.doc.data()['city'],
          companytype:element.payload.doc.data()['companytype'],
          educationLevel:element.payload.doc.data()['educationLevel'],
          educationYears:element.payload.doc.data()['educationYears'],
          enrollment:element.payload.doc.data()['enrollment'],
          experienceYears:element.payload.doc.data()['experienceYears'],
          majorDiscipline:element.payload.doc.data()['majorDiscipline'],
          rExperience:element.payload.doc.data()['rExperience'],
          result:element.payload.doc.data()['result'],
          state:element.payload.doc.data()['state'],
          street:element.payload.doc.data()['street'],
          yearsInCurrentJob:element.payload.doc.data()['yearsInCurrentJob'],
          uidUser:element.payload.doc.data()['uidUser'],
       }
       })
     })

  }
  details(key){
    this.router.navigate(['/employee/'+key])
  }
  delete(key){
    this.fs.collection("employees").ref.doc(key).delete()
  }
  setdataforUpdate(firstName,
    lastName,gender,age,
    trainingHours,key,
    companySize,companytype,
    educationLevel,
    educationYears,enrollment,
    experienceYears,majorDiscipline,
    rExperience,result,state,street,yearsInCurrentJob,city){
      this.dataupdate.key=key
      this.dataupdate.firstName=firstName
      this.dataupdate.lastName=lastName
      this.dataupdate.age=age
      this.dataupdate.gender=gender
      this.dataupdate.trainingHours=trainingHours
      this.dataupdate.companySize=companySize
      this.dataupdate.companytype=companytype
      this.dataupdate.educationLevel=educationLevel
      this.dataupdate.educationYears=educationYears
      this.dataupdate.enrollment=enrollment
      this.dataupdate.experienceYears=experienceYears
      this.dataupdate.majorDisciplin=majorDiscipline
      this.dataupdate.rExperience=rExperience
      this.dataupdate.result=result
      this.dataupdate.state=state
      this.dataupdate.street=street
      this.dataupdate.yearsInCurrentJob=yearsInCurrentJob
      this.dataupdate.city=city


    }
    update(){
      this.fs.collection("employees").ref.doc(this.dataupdate.key).update({
        firstName:this.dataupdate.firstName,
        lastName:this.dataupdate.lastName,
        age:this.dataupdate.age,
        city:this.dataupdate.city,
        gender:this.dataupdate.gender,
        trainingHours:this.dataupdate.trainingHours,
        companySize:this.dataupdate.companySize,
        companytype:this.dataupdate.companytype,
        educationLevel:this.dataupdate.educationLevel,
        educationYears:this.dataupdate.educationYears,
        enrollment:this.dataupdate.enrollment,
        experienceYears:this.dataupdate.experienceYears,
        majorDisciplin:this.dataupdate.majorDisciplin,
        rExperience:this.dataupdate.rExperience,
        result:this.dataupdate.result,
        state:this.dataupdate.state,
        street:this.dataupdate.street,
        yearsInCurrentJob:this.dataupdate.yearsInCurrentJob,
    
      }).then(()=>{
        this.messageupdate='updated successfully'
      })
    }
    
  // employeePage(employee:Employee){
  //   this.router.navigate(['/employee/',employee.age]); 
  // }
 
  // public dataSource: MatTableDataSource<Employee>;
  // private serviceSubscribe: Subscription;

  // edit(data: Employee) {
    // const dialogRef = this.dialog.open(PersonFormDialogComponent, {
    //   width: '400px',
    //   data: data
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.EmployeesService.edit(this.userId,data);
    //   }
    // });
  // }

  // delete(id: any) {
  //   const dialogRef = this.dialog.open(ConfirmationDialogComponent);

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.EmployeesService.deleteEmployee(this.userId,id);
  //     }
  //   });
  // }

  
  /**
   * initialize data-table by providing persons list to the dataSource.
   */
  ngOnInit(): void {
    // this.authService.getUser().subscribe(
    //   user => {
    //     this.userId = user.uid;
    //     console.log(this.userId + "sgsg");
    //     this.employeess$ = this.EmployeesService.getEmployees2(this.userId); 
  
    //     this.employeess$.subscribe(
    //       docs =>{
    //         console.log('init worked');
    //         console.log(docs );
    //         this.dataSource = new MatTableDataSource<Employee>();
    //         this.dataSource.data = [];
    //         for(let document of docs){
    //           const employee:Employee = document.payload.doc.data();
    //           employee.id = document.payload.doc.id; 

    //           this.dataSource.data.push(employee);
    //           this.employees$.next(this.dataSource.data) 
    //           this.serviceSubscribe=this.employees$.subscribe(res => {
    //             this.dataSource.data = res;
    //           });

    //         }
    //         this.ngAfterViewInit();
    //       }
    //     ) 
    //   }
    // )


  }
  // ngAfterViewInit(): void {
  //   console.log('paginate');
  //   this.dataSource.sort = this.sort;
  //   this.dataSource.paginator = this.paginator;

  // }

  //  ngOnDestroy(): void {
  //    this.serviceSubscribe.unsubscribe();
  //  }

  // private filter() {

  //   this.dataSource.filterPredicate = (data: Employee, filter: string) => {
  
  //     let find = true;
  
  //     for (var columnName in this.columnsFilters) {
  
  //       let currentData = "" + data[columnName];
  
  //       //if there is no filter, jump to next loop, otherwise do the filter.
  //       if (!this.columnsFilters[columnName]) {
  //         return;
  //       }
  
  //       let searchValue = this.columnsFilters[columnName]["contains"];
  
  //       if (!!searchValue && currentData.indexOf("" + searchValue) < 0) {
  
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  
  //       searchValue = this.columnsFilters[columnName]["equals"];
  
  //       if (!!searchValue && currentData != searchValue) {
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  
  //       searchValue = this.columnsFilters[columnName]["greaterThan"];
  
  //       if (!!searchValue && currentData <= searchValue) {
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  
  //       searchValue = this.columnsFilters[columnName]["lessThan"];
  
  //       if (!!searchValue && currentData >= searchValue) {
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  
  //       searchValue = this.columnsFilters[columnName]["startWith"];
  
  //       if (!!searchValue && !currentData.startsWith("" + searchValue)) {
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  
  //       searchValue = this.columnsFilters[columnName]["endWith"];
  
  //       if (!!searchValue && !currentData.endsWith("" + searchValue)) {
  //         find = false;
  //         //exit loop
  //         return;
  //       }
  //     }
  //     return find;
  
  //   };
  
  //   this.dataSource.filter = null;
  //   this.dataSource.filter = 'activate';
  
  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }
  
  /**
  
   * Create a filter for the column name and operate the filter action.
  
   */
  
  // applyFilter(columnName: string, operationType: string, searchValue: string) {
  
  //   this.columnsFilters[columnName] = {};
  //   this.columnsFilters[columnName][operationType] = searchValue;
  //   this.filter();
  // }
  
  /**
  
   * clear all associated filters for column name.
  
   */
  
  // clearFilter(columnName: string) {
  //   if (this.columnsFilters[columnName]) {
  //     delete this.columnsFilters[columnName];
  //     this.filter();
  //   }
  // }
}

