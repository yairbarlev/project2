import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import {ChatService,Message} from '../chat.service';
import {scan} from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';


declare const L: any;

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  email:any = "";
  afAuth: any;
  messages:Observable<Message[]>;
  formValue:string;
  datacontact={
  name:'',
  message:'',
  email:'',
  subject:''
}
messagesuccess
  constructor(public authService:AuthService,private chat:ChatService,private fs:AngularFirestore) { }

  ngOnInit() {
    this.messages=this.chat.conversation.asObservable().pipe(scan((acc,val)=>acc.concat(val)))
    
    
    if (!navigator.geolocation) {
      console.log('location is not supported');
    }
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      const latLong = [coords.latitude, coords.longitude];
      console.log(
        `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
      );
      let mymap = L.map('map').setView(latLong, 13);

      L.tileLayer(
        'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoid2lub3BhcGEiLCJhIjoiY2tuN3FudjZiMDdiaTJxcGk2YTJwZGVkZyJ9.Nuq_zzWmRc_ntpdOqojDUQ',
        {
          attribution:
            'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'your.mapbox.access.token',
        }
      ).addTo(mymap);

      let marker = L.marker(latLong).addTo(mymap);

      marker.bindPopup('<b>Hi</b>').openPopup();

      let popup = L.popup()
        .setLatLng(latLong)
        .setContent('I am Here')
        .openOn(mymap);
    });
    this.watchPosition();
  }
  sendMessage(){
    this.chat.converse(this.formValue);
  }
  watchPosition() {
    let desLat = 0;
    let desLon = 0;
    let id = navigator.geolocation.watchPosition(
      (position) => {
        console.log(
          `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
        );
        if (position.coords.latitude === desLat) {
          navigator.geolocation.clearWatch(id);
        }
      },
      (err) => {
        console.log(err);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      }
    );
  }

  sendcontact(){
    this.fs.collection("contact").add({
      name:this.datacontact.name,
      email:this.datacontact.email,
      subject:this.datacontact.subject,
      message:this.datacontact.message
    }).then(()=>{
      this.messagesuccess="data sent successfully"
    })
  }

}
