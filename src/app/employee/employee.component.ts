import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../interfaces/employee';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  id:string;
  countHappy:number=0;
  countSad:number=0;
  getdata:any
  useruid
  result:boolean;
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );


  constructor(private breakpointObserver: BreakpointObserver,private router:Router,private route:ActivatedRoute,private fs:AngularFirestore,private authservice:AuthService) {
    this.authservice.user.subscribe(user=>{
      this.useruid=user.uid
    })

    this.id = this.route.snapshot.params.id; 
    this.fs.collection("employees").ref.doc(this.id).get().then(data=>{

    this.getdata=data.data()
    console.log(this.getdata.result);
    if(this.getdata.result>=0.5){
      this.countHappy=this.countHappy+1;
      this.result=true;
      console.log(this.result);
    } 
    else{
      this.countSad=this.countSad+1;
      this.result=false;
      console.log(this.result);
    }    
    }).then(()=>{
      if(this.getdata.uidUser!==this.useruid){
        this.router.navigate(['/home'])
      }
    })

  }
}
