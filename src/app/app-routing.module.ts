import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { ResultComponent } from './result/result.component';
import { MiddleComponent } from './middle/middle.component';
import { AccountComponent } from './account/account.component';
import { SalesComponent } from './sales/sales.component';
import { EmployeeTableComponent } from './employee-table/employee-table.component';
import { DashComponent } from './dash/dash.component';
import { EmployeeComponent } from './employee/employee.component';
import { AuthGuardService } from './guard/auth-guard.service';
import { NoauthGuardService } from './guard/noauth-guard.service';
import { InformationComponent } from './information/information.component';


const routes: Routes = [

  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'books', component: BooksComponent ,canActivate:[AuthGuardService]},
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'classify/:network', component: ClassifyComponent },
  { path: 'city', component: CityFormComponent }, 
  { path: 'posts', component: PostsComponent},
  { path: 'login', component: LoginComponent,canActivate:[NoauthGuardService]},
  { path: 'signup', component: SignUpComponent,canActivate:[NoauthGuardService]},  
  { path: 'home', component: HomeComponent},  
  { path: 'form', component: FormComponent,canActivate:[AuthGuardService]}, 
  { path: 'result/:answer', component: ResultComponent}, 
  { path: 'middle/:companyType/:companySize', component: MiddleComponent },
  { path: 'account', component: AccountComponent,canActivate:[AuthGuardService]},
  { path: 'employeetable', component: EmployeeTableComponent,canActivate:[AuthGuardService]},
  { path: 'sale', component: SalesComponent},
  { path: 'dash', component: DashComponent,canActivate:[AuthGuardService]},
  { path: 'employee/:id', component: EmployeeComponent},
  { path: 'information', component: InformationComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
